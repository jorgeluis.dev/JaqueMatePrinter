# Jaque Mate Printer

Aplicación para generar el RIDE en Pdf a partir de un XML

Run:

$ gradle run

Generar archivo war con el comando:

$ gradle build

Cambiar el nombre al .war generado a "printer.war"

Para hacer un deploy en el servidor tomcat:

1. Hacer el deploy en el manager
2. Copiar el contenido del archivo web.xml de la carpeta WEB-INF en el directorio de ROOT, en su archivi respectivo
3. Copiar las carpetas necesarias del proyecto en el directorio del ROOT
4. Probar con los url propios de la aplicación, tiene que ser diferente al de ROOT "/"